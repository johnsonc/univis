# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import smart_str, smart_unicode

class CoreDepartment(models.Model):
    id = models.IntegerField(primary_key=True)
    name_id = models.IntegerField()
    description = models.TextField(blank=True, null=True)
    class Meta:
        db_table = 'core_department'
        verbose_name_plural='Departments'
        
    def __unicode__(self):
        return str(self.name_id)



class CoreInstitute(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255L)
    description = models.TextField(blank=True, null=True )
    rank = models.IntegerField(null=True, blank=True )
    score = models.FloatField(null=True, blank=True )
    url = models.CharField(max_length=255L, blank=True, null=True )
    address_id = models.IntegerField(null=True, blank=True )
    type = models.CharField(max_length=3L)
    verified_state = models.CharField(max_length=3L, blank=True, null=True )
    country_id = models.IntegerField(null=True, blank=True)
    country_rank = models.IntegerField(null=True, blank=True )
    class Meta:
        db_table = 'core_institute'
        verbose_name_plural='Institutes'

    def __unicode__(self):
        return self.name



class CoreInstitutealias(models.Model):
    id = models.IntegerField(primary_key=True)
    institute = models.ForeignKey(CoreInstitute, related_name='aliases')
    name = models.CharField(max_length=255L)
    ascii_name = models.CharField(max_length=255L, blank=True, null=True)
    verified_state = models.CharField(max_length=3L)
    class Meta:
        db_table = 'core_institutealias'
        verbose_name_plural='Institute Alias'

    def __unicode__(self):
        return self.institute.name


class CoreInstitutehasdepartment(models.Model):
    id = models.IntegerField(primary_key=True)
    institute = models.ForeignKey(CoreInstitute, related_name='departments')
    department = models.ForeignKey(CoreDepartment, related_name='institutealiases')
    rank = models.IntegerField()
    employer_score = models.DecimalField(null=True, max_digits=12, decimal_places=4, blank=True)
    academic_score = models.DecimalField(null=True, max_digits=12, decimal_places=4, blank=True)
    citation_score = models.DecimalField(null=True, max_digits=12, decimal_places=4, blank=True)
    grad_rank = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = 'core_institutehasdepartment'
        verbose_name_plural='Institute-Departments'

    def __unicode__(self):
        return self.institute.name + " - " + str(self.department.name_id).encode('utf-8')


class CoreTag(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=128L, unique=True)
    class Meta:
        db_table = 'core_tag'
        verbose_name_plural='Tags'

    def __unicode__(self):
        return self.name




class CoreTaghassubtags(models.Model):
    id = models.IntegerField(primary_key=True)
    tag = models.ForeignKey(CoreTag, related_name='child_tags')
    subtag = models.ForeignKey(CoreTag, related_name='parent_tags')
    alpha = models.DecimalField(max_digits=10, decimal_places=4)
    class Meta:
        db_table = 'core_taghassubtags'
        verbose_name_plural='SubTags'

    def __unicode__(self):
        return str(self.tag) + '-'+ str(self.subtag)

