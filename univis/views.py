from django.template import RequestContext, Context, Template
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
import json
from univis.models import CoreDepartment, CoreInstitute, CoreInstitutealias, CoreInstitutehasdepartment, CoreTag, CoreTaghassubtags

def index(request):
    """
    Index file
    """
    context = RequestContext(request)
    return render_to_response('index.html', context)

def univis(request):
    """
    University ranking grid
    """

    context = RequestContext(request)
    return render_to_response('univis.html', context)

def tag_tree(request):
    """
    Tag Display
    """
    context = RequestContext(request)
    return render_to_response('tag-tree.html', context)

def filter(request):
    """
    Ajax data filter handler
    """
    name = request.GET.get("name", "all")
    if name == "tb_univis":
        deptmap = {1: u'Mathematics',
                   2: u'Computer Science',
                   3: u'Physics',
                   4: u'Materials Science',
                   5: u'Chemical Engineering',
                   6: u'Civil Engineering',
                   7: u'Electrical Engineering',
                   8: u'Mechanical Engineering',
                   9: u'Biology',
                   10: u'Operations Research',
                   11: u'Aerospace Engineering',
                   12: u'Finance',
                   13: u'Statistics',
                   14: u'Chemistry',
                   15: u'Accounting',
                   16: u'Earth Science',
                   17: u'Economics',
                   18: u'Environmental Science',
                   19: u'Medicine',                   
                   20: u'Pharmacy'}            

        tableau = CoreInstitutehasdepartment.objects.prefetch_related('institute', 'department').all()
        data = [ {'id':obj.id,'name':obj.institute.name,'acad_score':float(obj.academic_score),'cite_score':float(obj.citation_score),'dept_name':deptmap[int(obj.department_id)],'emp_score':float(obj.employer_score),'grad_rank':float(obj.grad_rank),'ug_rank':float(obj.rank),'aliases':', '.join([ x[0] for x in obj.institute.aliases.values_list('name')])} for obj in tableau ]
        
        return  HttpResponse(json.dumps(data), "application/json")
    else:
        # returns the entire dataset
        return redirect('/static/data/univis.json') 
    
    
    
def tag_json(request):
        #topnodes = [ x for x in CoreTag.objects.all() if x.parent_tags.count() == 0 ]
    node_metadata = [ {'_id':obj.id, 'title': obj.name} for obj in CoreTag.objects.all() ]
    relations = [ {'from': obj.tag_id, 'to': obj.subtag_id, 'type':'has_subtag', 'value':1, 'alpha':float(obj.alpha) } for obj in CoreTaghassubtags.objects.all() ]
    
    return  HttpResponse(json.dumps({'node_metadata':node_metadata, 'relations':relations}), "application/json")
    
    
    


