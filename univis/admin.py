from django.contrib import admin
from univis.models import CoreInstitute, CoreDepartment, CoreInstitutealias, CoreInstitutehasdepartment, CoreTag, CoreTaghassubtags  


admin.site.register(CoreDepartment)
admin.site.register(CoreInstitute)
admin.site.register(CoreInstitutealias)
admin.site.register(CoreInstitutehasdepartment)
admin.site.register(CoreTag)
admin.site.register(CoreTaghassubtags)
