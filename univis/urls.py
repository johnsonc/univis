from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'univis.views.home', name='home'),
    # url(r'^univis/', include('univis.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
                      
                       url(r'^$', 'univis.views.index', name='index'),
                       url(r'^univis/', 'univis.views.univis', name='univis'),
                       url(r'^filter', 'univis.views.filter', name='filter'),    
                       url(r'^tag-tree/', 'univis.views.tag_tree', name='tag-tree'),
                       url(r'^tag-data/', 'univis.views.tag_json', name='tag-data'),


)
